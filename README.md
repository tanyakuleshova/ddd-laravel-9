    rename .env.example to .env

    composer install
    php artisan config:clear
    php artisan migrate
    sudo chmod -R 777 storage
    php artisan key:generate
    php artisan jwt:secret
    php artisan serve

    Project has ddd architecture. The example of domain component is located in app\Domains\Basic.
    To create new component you can simply copy Basic component and customize it.
    It's important to register new domain components in app\Domains\DomainServiceProvider.php;
