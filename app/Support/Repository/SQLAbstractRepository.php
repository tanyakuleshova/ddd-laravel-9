<?php

namespace App\Support\Repository;

use Carbon\Carbon;

abstract class SQLAbstractRepository implements RepositoryInterface
{
    const QTY = 20;
    public function getAll()
    {
        return static::MODEL::orderBy('id', 'desc')->with($this->relations)->get();
    }
    public function getAllPaginated()
    {
        return static::MODEL::orderBy('id', 'desc')->with($this->relations)->paginate(static::QTY);
    }
    public function firstByParams($params)
    {

        return static::MODEL::where($params)->with($this->relations)->first();
    }

    public function getSortedPaginated($field)
    {
        return static::MODEL::orderBy($field, 'asc')->with($this->relations)->paginate(static::QTY);
    }

    public function getById($id)
    {
        return static::MODEL::where('id', $id)->with($this->relations)->first();
    }

    public function delete($id)
    {
        return static::MODEL::where('id', $id)->delete();
    }

    public function deleteByParams($params){
        return static::MODEL::where($params)->delete();
    }
}
