<?php

namespace App\Support\Repository;

abstract class ElasticAbstractRepository implements RepositoryInterface
{
    public function getAll()
    {

    }

    public function getById($id)
    {

    }

    public function delete($id)
    {

    }
}
