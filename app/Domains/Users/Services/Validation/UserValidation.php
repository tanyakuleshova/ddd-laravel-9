<?php

namespace App\Domains\Users\Services\Validation;

use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class UserValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'first_name' => ['regex:'.User::NAME_REGEX, 'string', 'max:255'],
            'last_name' => ['nullable', 'string', 'max:255', 'alpha'],
            'gender'=> ['nullable', 'string', 'max:1'],
            'dob'=> ['nullable', 'date', 'date_format:Y-m-d'],
            'image' => ['nullable','image', 'mimes:jpg,png,jpeg,gif,svg'],
            '*.country' => ['nullable', 'string'],
            '*.city' => ['nullable', 'string'],
            '*.street' => ['nullable', 'string'],
            '*.zip_code' => ['nullable', 'string']
        ];
    }

    public function passwordRules(): array
    {
        return [
            'password' => ['required', 'string'],
            'new_password' => ['required', 'confirmed', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:12'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ];
    }

}
