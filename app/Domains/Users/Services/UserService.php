<?php
namespace App\Domains\Users\Services;

use App\Domains\Users\Models\Address;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserService
{
    public $user;
    public function __construct(
        protected User $userEntity,
        protected UserRepository $userRepository
    ){
        $this->user = auth('api')->user();
    }

    public function updatePassword($request): \Illuminate\Http\JsonResponse
    {
        if($this->user) {
            if (!$request->token = auth('api')->attempt($request->only('email', 'password'), ['exp' =>Carbon::now()->addHour()->timestamp])) {
                return response()->json(['success'=>false, 'error'=>"current_password_wrong"]);
            }
            $this->userEntity::store(['id'=>$this->user->id, 'password'=>$request->new_password]);

            $user = $this->userRepository->firstByParams(['id'=>$this->user->id]);
            return response()->json(['success'=>true, 'user'=>$user]);
        }
        return response()->json(['success'=>false, 'error'=>"user_not_found"]);
    }

    public function updateEmail($request): \Illuminate\Http\JsonResponse
    {
        $request->validate(['email'=>'required']);
        if($this->user) {
            $user = $this->userRepository->firstByParams(['email'=>$request->email]);
            if($user){
                return response()->json(['success'=>false, 'error'=>'email_already_exist']);
            }
            $this->userEntity::store(['id'=>$this->user->id, 'email'=>$request->email, 'email_verified_at'=>null]);
            auth('api')->logout();
            return response()->json(['success'=>true, 'user'=>null]);
        }
        return response()->json(['success'=>false, 'error'=>"user_not_found"]);
    }

    public function deleteUser(): \Illuminate\Http\JsonResponse
    {
        if($this->user){
            $this->userEntity::store(['id'=>$this->user->id, 'active'=>false, 'deleted_at' => Carbon::now()]);
            auth('api')->logout();
            return response()->json(['success'=>true]);
        }
        return response()->json(['success'=>false, 'Unauthorized'], 401);
    }

    public function getUser(): \Illuminate\Http\JsonResponse
    {
        if($this->user){
            $user = $this->userRepository->firstByParams(['id'=>$this->user->id]);
            return response()->json(['success' => true, 'user' => $user]);
        }

        return response()->json(['success' => false, 'error' => 'user_not_found']);
    }
}
