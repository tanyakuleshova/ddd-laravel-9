<?php

namespace App\Domains\Users\Http\Controllers;

use App\Domains\Users\Models\User;
use App\Domains\Users\Services\UserService;
use App\Domains\Users\Services\Validation\CredentialsValidation;
use App\Domains\Users\Services\Validation\NewPasswordValidation;
use App\Domains\Users\Services\Validation\UserValidation;
use App\Support\Http\Controller as BaseController;

use Illuminate\Http\Request;

/**
 *  @OA\PathItem(
 *      path="/api/user/get"
 *  ),
 * @OA\Get (path="/api/user/get",
 *     tags={"user"},
 *     summary="Route for getting user info and updating token",
 *     security={{ "apiAuth": {} }},
 *     description="Route for getting user info (updating token)",

 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example="{...}"),
 *        @OA\Property(property="token", type="string", example="xxxxx"),
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * )
 * )
 *  @OA\PathItem(
 *      path="/api/user/delete"
 *  ),
 * @OA\Delete (path="/api/user/delete",
 *     tags={"user"},
 *     summary="Route for deleting user",
 *     description="Route for deleting user",
 *      security={{ "apiAuth": {} }},
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true)
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * )
 * )
 *  @OA\PathItem(
 *      path="/api/user/change/password"
 *  ),
 * @OA\PUT (path="/api/user/change/password",
 *     tags={"user"},
 *     summary="Route for changing password",
 *     description="Route for changing password",
 *      security={{ "apiAuth": {} }},
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email", "password", "new_password", "new_password_confirmation"},
 *        @OA\Property(property="email", type="string", example="ttt@rrr.mail"),
 *        @OA\Property(property="password", type="string", example="rrr123$rrr"),
 *        @OA\Property(property="new_password", type="alphanumeric string with spec chars", example="111$eee12",  minLength=8, maxLength=12),
 *        @OA\Property(property="new_password_confirmation", type="string", example="111$eee12"),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example="{...}")
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="validation_error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example="{...}")
 *     )
 * )
 * )
 *  @OA\PathItem(
 *      path="/api/user/change/email"
 *  ),
 * @OA\PUT (path="/api/user/change/email",
 *     tags={"user"},
 *     summary="Route for changing email",
 *     description="Route for changing email",
 *      security={{ "apiAuth": {} }},
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email"},
 *        @OA\Property(property="email", type="string", example="ttt@rrr.mail")
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="null", example="null")
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="validation_error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example="{...}")
 *     )
 * )
 * )
 */
class UsersController extends BaseController
{
    public function __construct(
        protected UserValidation $validation,
        protected UserService $userService,
        protected User $entity
    ){}

    public function changePassword(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->passwordRules());
        return $this->userService->updatePassword($request);
    }

    public function changeEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->userService->updateEmail($request);
    }

    public function delete(): \Illuminate\Http\JsonResponse
    {
        return $this->userService->deleteUser();
    }

    public function get(): \Illuminate\Http\JsonResponse
    {
        return $this->userService->getUser();
    }
}
