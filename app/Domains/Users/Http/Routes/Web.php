<?php


use App\Domains\Users\Http\Controllers\UsersController;

$this->router->get('/index', [UsersController::class, 'index']);
