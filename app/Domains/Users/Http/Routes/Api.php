<?php


use App\Domains\Users\Http\Controllers\UsersController;

$this->router->middleware(['jwt.verify', 'request_handler'])->group(function ($router) {

$this->router->group(['prefix' => 'user'], function ($router) {
        $router->put('/change/password', [UsersController::class, 'changePassword']);
        $router->put('/change/email', [UsersController::class, 'changeEmail']);
        $router->delete('/delete', [UsersController::class, 'delete']);
    });
});

$this->router->get('/user/get', [UsersController::class, 'get']);
