<?php

namespace  App\Domains\Users\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class UserRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Users\Models\User';

    public array $relations = [
        'address'
    ];
    public array $search_fields = [

    ];
}
