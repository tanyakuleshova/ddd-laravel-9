<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('twitter_token')->after('active')->nullable();
            $table->text('facebook_token')->after('active')->nullable();
            $table->dropColumn('face_id_enabled');
            $table->dropColumn('finger_print_enabled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('twitter_token');
            $table->dropColumn('facebook_token');
            $table->boolean('face_id_enabled')->default(0);
            $table->boolean('finger_print_enabled')->default(0);
        });
    }
};
