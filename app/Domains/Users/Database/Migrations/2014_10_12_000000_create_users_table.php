<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')){
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('email')->unique();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->unsignedInteger('role_id')->nullable();
                $table->char('gender', 1)->nullable();
                $table->date('dob')->nullable();
                $table->string('phone')->nullable();
                $table->timestamp('phone_verified_at')->nullable();
                $table->boolean('face_id_enabled')->default(0);
                $table->boolean('finger_print_enabled')->default(0);
                $table->boolean('active')->default(1);
                $table->rememberToken();
                $table->dateTime('deleted_at')->nullable();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
    }
};
