<?php

namespace App\Domains\Users\Database\Seeders;

use App\Domains\Basic\Models\Basic;
use App\Domains\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        for($i=0; $i<10; $i++){
            $user = User::create([
                'first_name'=> fake()->firstName(),
                'last_name'=>fake()->lastName(),
                'email'=>fake()->email(),
                'password' => fake()->password(8,12)
            ]);
            $user->assignRole('User');
        }
        for($i=0; $i<10; $i++){
            $user = User::create([
                'first_name'=> fake()->firstName(),
                'last_name'=>fake()->lastName(),
                'email'=>fake()->email(),
                'password' => fake()->password(8,12),
                'email_verified_at'=>Carbon::now()
            ]);
            $user->assignRole('Admin');
        }
    }
}
