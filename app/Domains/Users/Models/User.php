<?php

namespace App\Domains\Users\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @OA\Schema(
 * @OA\Property(property="first_name", type="string", format="string", description="Contains letters and dashes", readOnly="false"),
 * @OA\Property(property="last_name", type="string", format="string", description="Contains only letters", readOnly="false"),
 * @OA\Property(property="email", type="string", format="email", description="User email", readOnly="false"),
 * @OA\Property(property="email_verified_at", type="string", format="date-time", description="Email verification timestamp", readOnly="true"),
 * @OA\Property(property="password", type="string", format="string", description="Contains spec characters, numbers, letters. Size 8-12 symbols", readOnly="false"),
 * @OA\Property(property="role_id", type="int", format="int", description="Every user has a role. For example: User, Super Admin, Admin", readOnly="true"),
 * @OA\Property(property="gender", type="char", format="string", description="values: m - male, f - femail", readOnly="true"),
 * @OA\Property(property="dob", type="string", format="date", description="Date of birth", readOnly="false"),
 * @OA\Property(property="phone", type="string", format="string", description="Contain only numbers", readOnly="false"),
 * @OA\Property(property="phone_verified_at", type="string", format="date-time", description="Phone verification timestamp", readOnly="true"),
 * @OA\Property(property="active", type="bool", format="bool", description="Show if user account is active", readOnly="false"),
 * @OA\Property(property="twitter_token", type="string", format="string", description="User facebook token", readOnly="false"),
 * @OA\Property(property="facebook_token", type="string", format="string", description="User twitter token", readOnly="false"),
 * @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true"),
 * @OA\Property(property="deleted_at", type="string", format="date-time", description="Soft delete timestamp", readOnly="true"),
 * )
 *
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject, HasMedia
{

    use SqlSaveTrait;
    use InteractsWithMedia;
//    use Searchable;
    use HasRoles;
    protected string $guard_name = 'web';


    public const PASSWORD_REGEX = '/^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/'; //alphanumeric with spec characters
    public const PHONE_REGEX = "/^\\d+$/"; //numeric
    public const NAME_REGEX = "/^[A-Za-z-]*$/"; //alpha with dash


    public $table = 'users';
    public $primaryKey = 'id';
    public $guarded = [];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function address(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Address::class);
    }

    public function registerMediaConversions(Media $media = null):void
    {
        $this->addMediaConversion('xs')
            ->width(100)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('sm')
            ->width(200)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('md')
            ->width(600)
            ->format('webp')
            ->nonQueued();

    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('user_icon')->singleFile();
        $this->addMediaCollection('user_images');
    }

}
