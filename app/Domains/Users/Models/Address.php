<?php

namespace App\Domains\Users\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="user_id", type="int", description="User id", readOnly="false"),
 * @OA\Property(property="country", type="string", description="Country name", readOnly="false"),
 * @OA\Property(property="city", type="string", description="City name", readOnly="false"),
 * @OA\Property(property="street", type="string", description="Street address (contains also house and flat)", readOnly="false"),
 * @OA\Property(property="zip_code", type="string", description="Zip code", readOnly="false"),
 * @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true"),
 * )
 *
 * @package App\Models
 */
class Address extends Model
{
    use SqlSaveTrait;
//    use Searchable;

    public $table = 'address';
    public $primaryKey = 'id';
    public $guarded = [];

}
