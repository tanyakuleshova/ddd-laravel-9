<?php


use App\Domains\Basic\Http\Controllers\BasicController;

$this->router->middleware(['guest'])->group(function ($router) {

    $router->group(['prefix' => 'basic'], function ($router) {
        $router->get('/get', [BasicController::class, 'index'])->name('basic');
    });

});


