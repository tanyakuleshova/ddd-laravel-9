<?php
namespace App\Domains\Basic\Http\Controllers\Traits;

trait Filtering
{
    protected function prepare_params($search): array
    {
        $params = explode('&', $search);
        $arr = [];
        foreach ($params as $param){
            $filter = explode('-', $param);
            if(count($filter)==2){
                $arr_ = [];
                if($filter[0] == 'sort'){
                    $p = explode('_', $filter[1]);
                    if(count($p)>1){
                        $arr_['by'] = $p[0];
                        $arr_['order'] = $p[1];
                        $arr['sort'] = $arr_;
                    }
                }else{
                    $arr_[] = $filter[0];
                    $arr_[] = $filter[1];
                    $arr['params'] = $arr_;
                }
            }
        }
        return $arr;
    }
}
