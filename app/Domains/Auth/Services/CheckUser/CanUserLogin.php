<?php
namespace App\Domains\Auth\Services\CheckUser;

use App\Domains\Users\Repository\SQLRepository\UserRepository;

class CanUserLogin
{
    public function check($user): array
    {
        if(!$user) return ['success'=> false,'error' => 'user_not_found'];

        if(!$user->email_verified_at) return ['success'=> false,'error' => 'email_not_verified'];

        if(!$user->active) return ['success'=> false, 'error' => 'user_is_not_active'];

        return ['success'=> true];
    }
}
