<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Jobs\SendEmailVerification;
use App\Domains\Auth\Models\EmailVerification;
use App\Domains\Auth\Repository\SQLRepository\EmailVerificationRepository;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;


class ProcessEmailVerification extends ProcessVerification
{
    public function __construct(
        protected EmailVerificationRepository $repository,
        protected UserRepository $userRepository

    ){}

    public function send($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'email'=>'required|string|email|max:255'
        ]);

        $verification = $this->createVerification($request->email);
        SendEmailVerification::dispatch($verification);

        return response()->json(['success'=>true]);
    }

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'code'=>'required',
            'email'=>'required|string|email|max:255'
        ]);

        $verification = $this->repository->firstByParams(['email'=>$request->email]);
        $verifyStatus = $this->verify($request->code, $verification);

        if(!$verifyStatus['success']){
            return response()->json($verifyStatus);
        }

        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        $user->email_verified_at = Carbon::now();
        $user->email = $request->email;
        $user->save();

        return response()->json(['success' => true]);
    }

    public function createVerification($email)
    {
        $code = $this->generateCode();
        $this->repository->deleteByParams(['email'=>$email]);
        return EmailVerification::store(['code'=>$code, 'email'=>$email]);
    }
}
