<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Traits\IsExpired;


abstract class ProcessVerification
{
    const DIGITS = 4;
    const EXPIRATION = 3600;
    const ATTEMPTS = 2;

    use IsExpired;

    public function generateCode(): string
    {
        return str_pad(rand(0, pow(10, self::DIGITS)-1), self::DIGITS, '0', STR_PAD_LEFT);
    }

    public function verify($code, $verification): array
    {
        if($verification){
            if($verification->code == $code){
                if($this->is_active($verification, self::EXPIRATION)){
                    if($verification->attempts < self::ATTEMPTS){
                        return ['success'=>true];
                    }
                    return ['success'=>false, 'error'=>'too_many_attempts'];
                }
                return ['success'=>false, 'error'=>'code_expired'];
            }
            $verification->attempts += 1;
            $verification->save();
            return ['success'=>false, 'error'=>'wrong_code'];
        }else{
            return ['success'=>false, 'error'=>'code_not_found'];
        }
    }
}
