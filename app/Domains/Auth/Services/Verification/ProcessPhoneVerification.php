<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Models\PhoneVerification;
use App\Domains\Auth\Repository\SQLRepository\PhoneVerificationRepository;
use App\Domains\SmsProvider\Services\SendSms\SendSMSTwilio;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;

class ProcessPhoneVerification extends ProcessVerification
{
    public function __construct(
        protected PhoneVerificationRepository $repository,
        protected UserRepository $userRepository,
        protected SendSMSTwilio $smsService

    ){}

    public function send($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'phone'=> ['required', 'regex:' . User::PHONE_REGEX, 'min:10, max:15'],
        ]);

        $verification = $this->createVerification($request->phone);
        //todo: check the format of phone (it must have a country code)
        $this->smsService::send('+'.$verification->phone, $verification->code);
        return response()->json(['success'=>true]);
    }

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'code'=>'required',
            'phone'=> ['required','regex:' . User::PHONE_REGEX, 'min:10, max:15'],
        ]);

        $verification = $this->repository->firstByParams(['phone'=>$request->phone]);
        $verifyStatus = $this->verify($request->code, $verification);

        if(!$verifyStatus['success']){
            return response()->json($verifyStatus);
        }

        $user = $this->userRepository->firstByParams(['phone'=>$request->phone]);
        $user->phone_verified_at = Carbon::now();
        $user->phone = $request->phone;
        $user->save();

        return response()->json(['success' => true]);
    }

    public function createVerification($phone)
    {
        $code = $this->generateCode();
        $this->repository->deleteByParams(['phone'=>$phone]);
        return PhoneVerification::store(['code'=>$code, 'phone'=>$phone]);
    }
}
