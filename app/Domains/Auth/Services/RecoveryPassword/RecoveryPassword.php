<?php

namespace App\Domains\Auth\Services\RecoveryPassword;

use App\Domains\Auth\Jobs\SendEmailResetLink;
use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\PasswordResetValidation;
use App\Domains\Auth\Traits\IsExpired;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RecoveryPassword
{
    use IsExpired;
    const LIFETIME = 3600;

    public function __construct(
        protected UserRepository $userRepository,
        protected User $entity,
        protected CanUserLogin $service,
        protected PasswordResetValidation $validation
    ){}

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());

        $passwordReset = DB::table('password_resets')->where('token', $request->token)->first();
        if(!$passwordReset){
            return response()->json(['success'=>false, 'error'=>'token_not_found'], 422);
        }
        if($passwordReset->token == $request->token){
            if($this->is_active($passwordReset, self::LIFETIME)){
                $user = $this->userRepository->firstByParams(['email'=>$passwordReset->email]);
                $this->entity::store(['id'=> $user->id, 'password'=>$request->password]);
//                DB::table('password_resets')->where('email', $passwordReset->email)->delete();
                return response()->json(['success'=>true]);
            }
            return response()->json(['success'=>false, 'error'=>'token_expired'], 422);
        }
        return response()->json(['success'=>false, 'error'=>'invalid_token'], 422);
    }

    public function send($request)
    {
        $request->validate(['email'=>'required']);

        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error']]);

        SendEmailResetLink::dispatch($this->createPasswordReset($request));

        return response()->json(['success'=>true]);
    }

    public function createPasswordReset($request)
    {
        $token = md5(uniqid());
        DB::table('password_resets')->where('email', $request->email)->delete();
        DB::table('password_resets')->insert([
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()],
        ]);
        return DB::table('password_resets')->where('email', $request->email)->first();
    }


}
