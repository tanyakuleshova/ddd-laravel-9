<?php

namespace App\Domains\Auth\Services\Validation;

use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class SocialRegisterValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'facebook_token' => ['required_without:twitter_token', 'string'],
            'twitter_token' => ['required_without:facebook_token', 'string'],
            'first_name' => ['required','regex:' . User::NAME_REGEX, 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255', 'alpha'],
            'phone' => ['regex:' . User::PHONE_REGEX, 'min:10, max:15'],
            'email' => 'required|string|email|max:255',
            'password' => ['required', 'confirmed', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:12'],
        ];
    }
}
