<?php

namespace  App\Domains\Auth\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class EmailVerificationRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Auth\Models\EmailVerification';

    public array $relations = [

    ];

    public array $search_fields = [

    ];
}
