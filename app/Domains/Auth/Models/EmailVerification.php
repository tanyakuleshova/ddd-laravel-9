<?php

namespace App\Domains\Auth\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;


class EmailVerification extends Model
{
    use SqlSaveTrait;
    public $table = 'email_verification';
    public $primaryKey = 'id';
    public $guarded = [];

}
