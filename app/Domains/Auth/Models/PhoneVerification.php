<?php

namespace App\Domains\Auth\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;


class PhoneVerification extends Model
{
    use SqlSaveTrait;
    public $table = 'phone_verification';
    public $primaryKey = 'id';
    public $guarded = [];

}
