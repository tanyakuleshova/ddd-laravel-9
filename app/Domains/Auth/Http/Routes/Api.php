<?php

use App\Domains\Auth\Http\Controllers\AdminLoginController;
use App\Domains\Auth\Http\Controllers\AdminResetPasswordController;
use App\Domains\Auth\Http\Controllers\LoginController;
use App\Domains\Auth\Http\Controllers\RegisterController;
use App\Domains\Auth\Http\Controllers\ResetPasswordController;
use App\Domains\Auth\Http\Controllers\VerificationController;

$this->router->middleware(['request_handler'])->group(function ($router) {

    $this->router->group(['prefix' => 'auth'], function ($router) {
        $router->post('/register', [RegisterController::class, 'register']);
        $router->post('/login', [LoginController::class, 'login']);
        $router->post('/logout', [LoginController::class, 'logout'])->middleware('jwt.verify');


        $router->group(['prefix' => 'verification'], function ($router) {
            $router->post('/email/send', [VerificationController::class, 'sendEmail'])->middleware('is_user_already_verified');
            $router->post('/email/verify', [VerificationController::class, 'verifyEmail'])->middleware('is_user_already_verified');
            $router->post('/phone/send', [VerificationController::class, 'sendSms'])->middleware('is_user_already_verified');
            $router->post('/phone/verify', [VerificationController::class, 'verifyPhone'])->middleware('is_user_already_verified');
        });

        $router->group(['prefix' => 'reset'], function ($router) {
            $router->post('/send_email_link', [ResetPasswordController::class, 'sendResetLink']);
            $router->put('/password', [ResetPasswordController::class, 'resetPassword']);
        });
    });



    $this->router->group(['prefix' => 'admin'], function ($router) {

        $this->router->group(['prefix' => 'auth'], function ($router) {
            $router->post('/login', [AdminLoginController::class, 'login']);
            $router->post('/logout', [AdminLoginController::class, 'logout']);

            $router->group(['prefix' => 'reset'], function ($router) {
                $router->post('/send_email_link', [AdminResetPasswordController::class, 'sendResetLink']);
                $router->put('/password', [AdminResetPasswordController::class, 'resetPassword']);
            });
        });
    });

});




