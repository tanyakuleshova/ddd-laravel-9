<?php

namespace App\Domains\Auth\Http\Middleware;

use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Closure;

class IsUserAlreadyVerified
{
    public function handle($request, Closure $next): mixed
    {
        if($request->email){
            $user = (new UserRepository)->firstByParams(['email'=>$request->email]);
            if($user){
                if($user->email_verified_at) return response()->json(['success'=>false, 'error'=>'email_already_verified'], 422);
            }else{
                return response()->json(['success'=>false, 'error'=>'user_not_found'], 422);
            }
        }
        if($request->phone){
            $user = (new UserRepository)->firstByParams(['phone'=>$request->phone]);
            if($user){
                if($user->phone_verified_at) return response()->json(['success'=>false, 'error'=>'phone_already_verified'], 422);
            }else{
                return response()->json(['success'=>false, 'error'=>'user_not_found'], 422);
            }
        }

        return $next($request);
    }
}
