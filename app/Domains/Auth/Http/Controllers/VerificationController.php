<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Auth\Services\Verification\ProcessPhoneVerification;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

class VerificationController extends BaseController
{
    public function __construct(
        protected ProcessEmailVerification $emailVerification,
        protected ProcessPhoneVerification $phoneVerification,
    ){}

    public function sendEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->emailVerification->send($request);
    }

    public function verifyEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->emailVerification->process($request);
    }

    public function sendSms(Request $request): \Illuminate\Http\JsonResponse
    {
       return $this->phoneVerification->send($request);
    }

    public function verifyPhone(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->phoneVerification->process($request);
    }

}
