<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\RecoveryPassword\RecoveryPassword;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;
class AdminResetPasswordController extends BaseController
{
    public function __construct(
        protected RecoveryPassword $recoveryPasswordService,
    ){}

    public function sendResetLink(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->recoveryPasswordService->send($request);
    }

    public function resetPassword(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->recoveryPasswordService->process($request);
    }
}
