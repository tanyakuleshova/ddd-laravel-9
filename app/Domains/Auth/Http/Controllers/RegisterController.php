<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

class RegisterController extends BaseController
{
    public function __construct(
        protected UserRepository $repository,
        protected User $entity,
        protected RegisterValidation $validation,
        protected ProcessEmailVerification $emailVerification
    ){}

    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->validate($this->validation->rules());
        $user = $this->repository->firstByParams(['email'=>$request->email]);
        if($user){
            if(!$user->active){
                $data['id'] = $user->id;
                $data['active'] = true;
                $user = $this->entity::store($data);
            }else{
                return response()->json(['success' => false, 'error'=>'email_already_exist']);
            }
        }else{
            $user = $this->entity::store($data);
            $user->assignRole('User');
        }

        return response()->json(['success' => true, 'user'=>$user]);

    }


}
