<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\LoginValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use App\Support\Http\Controller as BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminLoginController extends BaseController
{
    public function __construct(
        protected UserRepository $repository,
        protected User $entity,
        protected LoginValidation $validation,
        protected CanUserLogin $service,

    ){}

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());
        if (!$request->token = auth('api')->attempt($request->only('email', 'password'), ['exp' =>Carbon::now()->addHour()->timestamp])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = $this->repository->firstByParams(['email'=>$request->email]);
        if(!($user->can('super') || $user->can('admin'))){
            return response()->json(['success'=>false, 'error'=>true, 'message'=>'Permission denied.'], 401);
        }
        $user = auth('api')->user();

        $payload = auth('api')->payload()->toArray();

        return response()->json([
            'token' => $request->token,
            'token_type' => 'bearer',
            'expires_in' => $payload['exp'] ?? '',
            'user' => $user,
            'success'=>true
        ]);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth('api')->logout();
        return response()->json(['success'=>true]);
    }
}
