<?php
namespace App\Domains\Media\Services;
use App\Domains\Media\Models\Media;
use App\Domains\Media\Services\Validation\UploaderValidation;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;


class UploaderService
{

    protected $validation;
    protected array $media;

    public function __construct(){
        $this->validation = new UploaderValidation();
    }
    public function uploadMedia($request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());
        try {
            $this->uploadFromRequest($request->entity, $request->collection, 'files');
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
        return response()->json(['success' => true, 'url' =>  $this->media]);
    }

    public function uploadFromRequest($entity, $collection, $parameter): array
    {
        $this->media = [];
        $entity->addMultipleMediaFromRequest([$parameter])
            ->each(function ($fileAdder) use ($collection) {
                $media = $fileAdder->toMediaCollection($collection);
                $path = storage_path() . "/app/media" . $media->partOfPath();
                try{
                    Image::load($path)->fit(Manipulations::FIT_MAX, Media::MAX_WIDTH, Media::MAX_HEIGHT)->save($path);
                }catch (\Exception $e){

                }
                $this->media[] = $media->path();
            });
        return $this->media;
    }
}
