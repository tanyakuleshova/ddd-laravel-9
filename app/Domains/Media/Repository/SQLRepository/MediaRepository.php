<?php

namespace  App\Domains\Media\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class MediaRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Media\Models\Media';

    public array $relations = [

    ];
}
