<?php

namespace App\Domains\Media\Http\Controllers;
use App\Domains\Media\Services\UploaderService;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

class MediaController extends BaseController
{
    public function __construct(
        protected UploaderService $service
    ){}

    public function uploadMedia(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->service->uploadMedia($request);
    }
}
