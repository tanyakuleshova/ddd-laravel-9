<?php

use App\Domains\Media\Http\Controllers\MediaController;
$this->router->group(['middleware' => ['request_handler', 'jwt.verify'], 'prefix' => 'media'], function ($router) {
    $router->post('/upload', [MediaController::class, 'uploadMedia']);
});
