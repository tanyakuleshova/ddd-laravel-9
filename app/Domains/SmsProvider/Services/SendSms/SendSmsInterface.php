<?php

namespace App\Domains\SmsProvider\Services\SendSms;

interface SendSmsInterface
{
    public static function send($phone, $message);
}
