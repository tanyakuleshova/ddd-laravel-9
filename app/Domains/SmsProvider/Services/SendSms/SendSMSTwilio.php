<?php

namespace App\Domains\SmsProvider\Services\SendSms;

use Aloha\Twilio\Twilio;

class SendSMSTwilio implements SendSmsInterface
{
    public static function send($phone, $message): \Illuminate\Http\JsonResponse
    {
        try {
            $twilio = new Twilio(env('TWILIO_ID'), env('TWILIO_TOKEN'),  env('TWILIO_FROM'));
            $twilio->message($phone, $message);

        }catch (\Exception $e){
            return response()->json(['success'=>false, 'error'=>$e->getMessage()], 500);
        }
        return response()->json(['success'=>true]);
    }
}
