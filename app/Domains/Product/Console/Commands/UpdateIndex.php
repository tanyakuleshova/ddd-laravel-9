<?php

namespace App\Domains\Product\Console\Commands;

use App\Domains\Product\Models\Product;
use Illuminate\Console\Command;

class UpdateIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Product update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $pr = Product::all();
        foreach ($pr as $p){
//            if($p->id > 77993){
                $p->save();
                dump($p->id);
//            }
        }
        dump(count($pr));

    }

}
