<?php

namespace App\Domains\Product\Http\Controllers;

use App\Domains\Product\Repository\SQLRepository\ProductRepository;
use App\Support\Http\Controller as BaseController;

class ProductController extends BaseController
{
    public function __construct(
        protected ProductRepository $repository
    ){}

    public function index($id)
    {

    }
}
