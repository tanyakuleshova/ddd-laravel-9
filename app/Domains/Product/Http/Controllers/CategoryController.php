<?php

namespace App\Domains\Product\Http\Controllers;

use App\Domains\Product\Repository\SQLRepository\CategoryRepository;
use App\Domains\Product\Repository\SQLRepository\ProductRepository;
use App\Support\Http\Controller as BaseController;

class CategoryController extends BaseController
{
    public function __construct(
        public ProductRepository $productRepository,
        public CategoryRepository $categoryRepository,
    ) {}

    public function index()
    {
        // or return response()->json([]);
        return view('product::themes/auto/main');
    }

}
