<?php

use App\Domains\Product\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$this->router->group(['prefix' => ''], function ($router) {
    $router->get('/', [CategoryController::class, 'index'])->name('home');
//    $router->get('category/{id}/{link}/{filters?}', [CategoryController::class, 'index'])->name('category');
//    $router->post('category/{id}/{link}/{filters?}', [CategoryController::class, 'index']);
//    $router->get('product/{id}/{link}', [ProductController::class, 'index'])->name('product');
});

