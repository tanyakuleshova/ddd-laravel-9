@foreach($all_filters as $attribute)
{{--    @if($attribute['key'] !== 'Артикул' && $attribute['key'] !== 'vendor code')--}}
        <h1 class="text-lg py-2">{!! $attribute['key'] !!}</h1>
        <div class="my-4" style="max-height: 250px; overflow-y: auto">
            @if($attribute['values'] ?? '')
                @foreach($attribute['values'] as $k => $value)
                    <div class="text-md">
                        <input onchange="choose_filter(this)" data-atr_id="{{$attribute['atr_id']}}" data-atr_val="{{$k}}" style="accent-color: #fce263 !important;" class="filter w-4 h-4 text-primary focus:ring-0 focus:outline-0 outline-0 focus:ring-offset-0 bg-gray-200 rounded border-gray-300 my-2" type="checkbox">
                        <span class="ml-2">{!! $value['name'] ?? ''!!} ({{$value['doc_count'] ?? ''}})</span>
                    </div>
                @endforeach
            @endif
        </div>
{{--    @endif--}}
@endforeach
