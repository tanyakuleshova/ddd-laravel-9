@extends('product::themes.auto.layout')
@section('content')
    @include('product::themes.auto.header')
    <div class="max-w-screen-xl m-auto">
        <h1 class="text-2xl">{{$product->lang->name}}</h1>
        <div>
            <div class="flex my-4">
                <div class="swiper  border border-gray-200 rounded w-1/3">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach($product->getMedia('collection') as $media)
                            <div class="swiper-slide">
                                <img class="w-full" src="{{$media->getUrl()}}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev" style="width: 10px; color: #b4adad"></div>
                    <div class="swiper-button-next color-primary-light" style="width: 10px; color: #b4adad""></div>
                </div>
                <div class="w-2/3 pl-4 flex justify-end">
                    <div class="w-2/3 text-gray-700">
                        <div class="text-2xl">{{$product->price}} @lang('грн')</div>
                        <div class="text-2xl">@lang('Артикул') {{$product->reference}}</div>
                        <div class="text-center cursor-pointer my-2 p-2 rounded bg-primary">@lang('Додати в кошик')</div>
                    </div>
                </div>


            </div>
        <div class="my-8">
            <div class="text-xl mb-2">@lang('Параметри')</div>
            @foreach($product->attributes as $attribute)
               <div class="flex text-gray-700">
                   <div class="mr-2">{{$attribute->lang_name->name}}</div>
                   <div>{{$attribute->lang_value->name}}</div>
               </div>
            @endforeach
        </div>
        @if($product->lang->description)
            <div class="text-gray-700">
                <div class="text-xl mb-2">@lang('Опис')</div>
                {!! $product->lang->description !!}
            </div>
        @endif
        </div>

    </div>

    <script>

    </script>
@endsection

