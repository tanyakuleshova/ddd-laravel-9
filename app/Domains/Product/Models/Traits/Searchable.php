<?php
namespace App\Domains\Product\Models\Traits;

use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait Searchable
{
    protected $elasticsearch;
    protected $index;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->setHosts([env('ELASTIC_HOST')])->build();
        $this->index = Str::lower(__CLASS__);

    }
    public function search($query): array
    {

        $params = [
            'index' => 'product',
            'body' => [
                'size' => 500,
                'query' => [
                    'wildcard' => [
                        "langs.name" => [
                            "value" => "*$query*"
                        ]
                    ]
                ]
            ]
        ];
        $results = $this->elasticsearch->search($params)['hits']['hits'] ?? [];
        return Arr::pluck($results, '_id');
    }
}
