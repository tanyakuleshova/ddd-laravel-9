<?php
namespace App\Domains\Product\Models\Traits;

trait SqlAttributeDescSaveTrait
{
    public static function store($validated)
    {
        $class = __CLASS__;
        if(isset($validated['id'])){
            $model = $class::find((int)$validated['id']) ?? new $class();
        }elseif(isset($validated['attribute_id'])){
            $model = $class::where('attribute_id', (int)$validated['attribute_id'])->where('lang', $validated['lang'])->first() ?? new $class();
        } else{
            $model = new $class();
        }

        foreach ($validated as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;
    }
}
