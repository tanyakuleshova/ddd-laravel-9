<?php
namespace App\Domains\Product\Models\Traits;

trait SqlAttributeProductSaveTrait
{
    public static function store($validated)
    {
        $class = __CLASS__;
        if(isset($validated['id'])){
            $model = $class::find((int)$validated['id']) ?? new $class();
        }elseif(isset($validated['product_id'])){
            $model = $class::where('product_id', (int)$validated['product_id'])
                ->where('attribute_val_id', $validated['attribute_val_id'])->first() ?? new $class();
        } else{
            $model = new $class();
        }

        foreach ($validated as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;
    }
}
