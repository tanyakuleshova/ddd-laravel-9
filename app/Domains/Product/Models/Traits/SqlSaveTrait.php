<?php
namespace App\Domains\Product\Models\Traits;

trait SqlSaveTrait
{
    public static function store($validated)
    {
        $class = __CLASS__;
        if(isset($validated['id'])){
            $model = $class::find((int)$validated['id']) ?? new $class();
        }else{
            $model = new $class();
        }

        foreach ($validated as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;
    }
}
