<?php
namespace App\Domains\Product\Models\Traits;

trait SqlCategoryDescSaveTrait
{
    public static function store($validated)
    {
        $class = __CLASS__;
        if(isset($validated['category_id'])){
            $model = $class::find((int)$validated['category_id']) ?? new $class();
        }else{
            $model = new $class();
        }

        foreach ($validated as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;
    }
}
