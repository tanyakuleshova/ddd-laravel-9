<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class AttributeValue extends Model
{
    use SqlSaveTrait;
    public $table = 'attribute_value';
    public $primaryKey = 'id';
    public $guarded = [];

    public function lang_names()
    {
        return $this->hasMany(AttributeDesc::class, 'attribute_id', 'attribute_id');
    }

    public function lang_values()
    {
        return $this->hasMany(AttributeValueDesc::class, 'attribute_val_id', 'id');
    }

    public function lang_value()
    {
        return $this->hasOne(AttributeValueDesc::class, 'attribute_val_id', 'id')->where('lang', App::getLocale());
    }

    public function lang_name()
    {
        return $this->hasOne(AttributeDesc::class, 'attribute_id', 'attribute_id')->where('lang', App::getLocale());
    }

}
