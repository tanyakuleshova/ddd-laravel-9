<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlAttributeDescSaveTrait;
use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

class AttributeDesc extends Model
{
    use SqlSaveTrait;
    public $table = 'attribute_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
