<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlCategoryDescSaveTrait;
use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

class CategoryDesc extends Model
{
    use SqlSaveTrait;

    public $table = 'category_desc';
    public $primaryKey = 'id';
    public $guarded = [];

   public function category()
   {
       return $this->belongsTo(Category::class);
   }
}
