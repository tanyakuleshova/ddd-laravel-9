<?php

namespace App\Domains\Product\Models;

use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Product extends Model implements HasMedia
{
    use InteractsWithMedia;
    use SqlSaveTrait;
    use Searchable;

    public $table = 'product';
    public $primaryKey = 'id';
    public $guarded = [];

    public function toSearchableArray(): array
    {
        $with = [
            'attributes.lang_names', 'attributes.lang_values', 'category.langs', 'langs'
        ];
        $this->loadMissing($with);
        return $this->toArray();
    }

    public function attributes()
    {
        return $this->hasManyThrough(AttributeValue::class, AttributeProduct::class,
            'product_id', 'id', 'id', 'attribute_val_id');
    }

    public function lang()
    {
        return $this->hasOne(ProductDesc::class, 'product_id', 'id')->where('lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(ProductDesc::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('xs')
            ->width(100)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('sm')
            ->width(200)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('md')
            ->width(600)
            ->format('webp')
            ->nonQueued();

    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('collection');
    }
}
