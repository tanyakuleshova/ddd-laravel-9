<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlAttributeProductSaveTrait;
use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

class AttributeProduct extends Model
{
    use SqlSaveTrait;

    public $table = 'attribute_product';
    public $primaryKey = 'id';
    public $guarded = [];

    public function value(){
        return $this->hasOne(AttributeValue::class, 'id', 'attribute_val_id');
    }

}
