<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

class AttributeValueDesc extends Model
{
    use SqlSaveTrait;
    public $table = 'attribute_value_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function attribute_value(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AttributeValue::class);
    }
}
