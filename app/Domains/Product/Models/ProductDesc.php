<?php

namespace App\Domains\Product\Models;
use App\Domains\Product\Models\Traits\SqlProductDescSaveTrait;
use Illuminate\Database\Eloquent\Model;


class ProductDesc extends Model
{
    use SqlProductDescSaveTrait;
    public $table = 'product_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
