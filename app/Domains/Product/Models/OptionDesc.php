<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

class OptionDesc extends Model
{
    use SqlSaveTrait;

    public $table = 'option_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function option()
    {
        return $this->belongsTo(Option::class);
    }

    public function product()
    {
        return $this->hasManyThrough(Product::class, OptionVal::class);
    }
}
