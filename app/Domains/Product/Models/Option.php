<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Option extends Model
{
    use SqlSaveTrait;

    public $table = 'option';
    public $primaryKey = 'id';
    public $guarded = [];

    public function lang()
    {
        return $this->hasOne(OptionDesc::class, 'option_id', 'id')
            ->where('lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(OptionDesc::class, 'option_id', 'id');
    }

    public function options()
    {
        $options = [];
        foreach (OptionDesc::where('lang', App::getLocale())->get() as $lang){
            $options[$lang->option_id] = $lang->name;
        }
        return $options;
    }
}
