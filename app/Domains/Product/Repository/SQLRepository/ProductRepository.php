<?php

namespace  App\Domains\Product\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class ProductRepository extends SQLAbstractRepository
{

    const MODEL = 'App\Domains\Product\Models\Product';
    const QTY = 21;
    public array $relations = [
        'langs', 'attributes.value.lang', 'attributes.value.attribute.lang'
    ];
    public array $search_fields = [

    ];
    public function getByParamsPaginated($params)
    {
        return self::MODEL::where($params)->paginate(self::QTY);
    }
    public function getByIdsPaginated($ids)
    {
        return self::MODEL::whereIn('id', $ids)->paginate(self::QTY);
    }
    public function getByIds($ids, $qty)
    {
        return self::MODEL::whereIn('id', $ids)->limit($qty)->get();
    }
}
