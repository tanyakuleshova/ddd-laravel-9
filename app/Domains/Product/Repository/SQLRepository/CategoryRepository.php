<?php

namespace  App\Domains\Product\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class CategoryRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Product\Models\Category';

    public array $relations = [
        'lang', 'products', 'children.lang', 'children.children'
    ];

    public function main()
    {
        return self::MODEL::where('id', 1)->with($this->relations)->first();
    }
    public function menu_categories()
    {
        return self::MODEL::where('parent_id', 1)->where('active', 1)->with($this->relations)->get();
    }
}
