<?php

namespace App\Domains;

use App\Domains\Auth\Providers\AuthServiceProvider;
use App\Domains\Basic\Providers\BasicServiceProvider;
use App\Domains\Media\Providers\MediaServiceProvider;
use App\Domains\Product\Providers\ProductServiceProvider;
use App\Domains\Users\Providers\UserServiceProvider;
use Illuminate\Support\AggregateServiceProvider;

class DomainServiceProvider extends AggregateServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        BasicServiceProvider::class,
        UserServiceProvider::class,
        AuthServiceProvider::class,
        MediaServiceProvider::class,
        ProductServiceProvider::class
    ];
}
