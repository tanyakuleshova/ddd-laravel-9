<?php

namespace App\Core\Providers;

use App\Domains\Product\Services\Filtering\ElasticFiltering;
use App\Domains\Product\Services\Filtering\FilteringInterface;
use App\Domains\Product\Services\Searching\ElasticSearching;
use App\Domains\Product\Services\Searching\SearchingInterface;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use PDO;
use Illuminate\Support\ServiceProvider;
use Illuminate\Mail\MailServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path('Support/Helpers').DIRECTORY_SEPARATOR.'*.php') as $filename) {
            require_once $filename;
        }
        $this->app->bind(Client::class, function(){
            return ClientBuilder::create()
                ->setHosts([env('ELASTIC_HOST')])
                ->build();
        });

        $this->app->bind(
            SearchingInterface::class,
            function () {
                return new ElasticSearching();
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function registerHelpers()
    {
        foreach (glob(app_path('Support/Helpers').DIRECTORY_SEPARATOR.'*.php') as $filename) {
            require_once $filename;
        }
    }
}
