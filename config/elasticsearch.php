<?php

return [
    'host' => env('ELASTICSEARCH_HOST'),
    'user' => '',
    'password' => '',
    'cloud_id' => '',
    'api_key' => '',
    'indices' => [

        'settings' => [
            'default' => [
                'number_of_shards' => 1,
                'number_of_replicas' => 0,
            ],
        ]
    ],
];
