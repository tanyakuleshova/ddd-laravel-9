<?php

namespace Tests\Unit;

use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\LoginValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

use Tests\TestCase;
class LoginTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public $userRepository;
    public $entity;
    public $validation;
    public $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
        $this->entity = new User();
        $this->validation = new LoginValidation();
        $this->service = new CanUserLogin();
        Artisan::call('db:seed');
    }

    /** @test */
    public function login_user_with_not_verified_email()
    {
        $user = $this->create_user();

        $request = new Request(['email' => $user->email, 'password' => 'test$6789']);

        $request->validate($this->validation->rules());

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();
        $status = $this->service->check($user);

        $this->assertEquals($status['error'], 'email_not_verified');
    }

    /** @test */
    public function login_user_successfully()
    {
        $user = $this->create_user();
        $user->email_verified_at = Carbon::now();
        $user->save();
        $request = new Request(['email' => 'test@test', 'password' => 'test$6789']);

        $request->validate($this->validation->rules());

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();
        $status = $this->service->check($user);

        $this->assertEquals($status['success'], true);
    }

    /** @test */
    public function login_user_to_admin_successfully()
    {
        $user = $this->create_user();
        $user->assignRole('Admin');
        $user->email_verified_at = Carbon::now();
        $user->save();
        $request = new Request(['email' => 'test@test', 'password' => 'test$6789']);
        $request->validate($this->validation->rules());

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'), ['exp' =>Carbon::now()->addHour()->timestamp])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        if(!($user->can('super') || $user->can('admin'))){
            return response()->json(['success'=>false, 'error'=>true, 'message'=>'Permission denied.'], 401);
        }
        $user = auth('api')->user();
        $status = $this->service->check($user);

        $this->assertEquals($status['success'], true);
    }

    public function create_user()
    {
        $request = new Request([
            'email' => 'test@test',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069'
        ]);

        $data = $request->validate($this->validation->rules());
        $user = $this->entity::store($data);
        $user->assignRole('User');

        return $user;
    }

}
