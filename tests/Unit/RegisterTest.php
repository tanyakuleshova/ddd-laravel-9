<?php

namespace Tests\Unit;


use App\Domains\Auth\Http\Requests\RegisterRequest;
use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;

use Illuminate\Validation\ValidationException;
use Tests\TestCase;
class RegisterTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public $userRepository;
    public $entity;
    public $validation;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
        $this->entity = new User();
        $this->validation = new RegisterValidation();
        Artisan::call('db:seed');
    }

    /** @test */
    public function register_user_successfully()
    {
        $request = new Request([
            'email' => 'test@www',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069'
        ]);

//        $request->setContainer(app())
//            ->validateResolved();

        $data = $request->validate($this->validation->rules());
        $user = $this->entity::store($data);
        $user->assignRole('User');

        $this->assertEquals($user->can('user'), true);
        $this->assertEquals($user->email, 'test@www');
    }

    /** @test */
    public function register_user_with_validation_errors()
    {
        $request = new Request([
            'email' => 'test',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test',
            'password_confirmation' => 'tes',
            'phone' => '0937725069'
        ]);

        try {
//            $request->setContainer(app())
//                ->setRedirector(app(Redirector::class))
//                ->validateResolved();
//            $request->validated();
            $data = $request->validate($this->validation->rules());
        }catch(\Exception $exception){

        }

        $this->assertEquals(isset($exception), true);

    }

}
