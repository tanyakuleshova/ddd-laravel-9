<?php

namespace Tests\Unit;

use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Users\Models\Address;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use App\Domains\Users\Services\UserService;
use App\Domains\Users\Services\Validation\CredentialsValidation;
use App\Domains\Users\Services\Validation\NewPasswordValidation;
use App\Domains\Users\Services\Validation\UserValidation;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ChangeUserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public $userEntity;
    public $addressEntity;
    public $validation;
    public $registeValidation;
    public $passwordValidation;
    public $userRepository;
    public $userService;


    public function setUp(): void
    {
        parent::setUp();

        $this->userEntity = new User();
        $this->addressEntity = new Address();
        $this->validation = new UserValidation();
        $this->registeValidation = new RegisterValidation();
        $this->passwordValidation = new NewPasswordValidation();
        $this->userRepository = new UserRepository();
        $this->userService = new UserService($this->addressEntity, $this->userEntity, $this->userRepository);

        Artisan::call('db:seed');
    }

    /** @test */
    public function change_user_name_address_successfully()
    {
        $user = $this->create_user();
        $request = new Request(['email'=>$user->email, 'password'=>'test$6789']);

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();

        $this->assertEquals($user->first_name, 'Test');

        $request = new Request([
            "first_name" => "Sasha",
            "last_name" => "Milovski",
            "gender" => "m",
            "dob" => "1993-08-04",

            "address" => [
                "country" => "UAE",
                "city" => "Dubai",
                "street" => "Street",
                "zip_code" => "121212"
            ]
        ]);
        $data = $request->validate($this->validation->rules());

        if($user){
            $address = $data['address'] ?? [];
            if(!empty($address)){
                $address['user_id'] = $user->id;
                if($user->address){
                    $address['id'] = $user->address->id;
                }
                $this->addressEntity::store($address);
            }
            $data['id'] = $user->id;
            unset($data['address']);
            $this->userEntity::store($data);
        }

        $user = $this->userRepository->firstByParams(['id'=>$user->id]);


        $this->assertEquals($user->first_name, 'Sasha');
    }

    /** @test */
    public function change_user_info_failed()
    {
        $user = $this->create_user();
        $request = new Request(['email'=>$user->email, 'password'=>'test$6789']);

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();

        $this->assertEquals($user->first_name, 'Test');

        $request = new Request([
            "first_name" => "Sasha",
            "last_name" => "Milovski01",
            "gender" => "m",
            "dob" => "1993-08-04",

            "address" => [
                "country" => 2,
                "city" => "Dubai",
                "street" => "Street",
                "zip_code" => "121212"
            ]
        ]);
        try{
            $data = $request->validate($this->validation->rules());
        }catch (\Exception $exception){

        }
        $this->assertEquals(isset($exception), true);
    }

    /** @test */
    public function change_user_password_successfully()
    {
        $user = $this->create_user();
        $request = new Request(['email'=>$user->email, 'password'=>'test$6789']);

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();
        $request = new Request([
            "new_password" => "12387@rrr",
            "new_password_confirmation" => "12387@rrr",
            "password" => 'test$6789',
            "email"=>"test@test"
        ]);
        $request->validate($this->passwordValidation->rules());
        $this->userService->user = $user;
        $response = $this->userService->updatePassword($request);

        $request = new Request(['email'=>$user->email, 'password'=>'12387@rrr']);
        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $this->assertEquals(json_decode($response->getContent())->success, true);

    }

    /** @test */
    public function change_user_email_successfully()
    {
        $user = $this->create_user();
        $request = new Request(['email'=>$user->email, 'password'=>'test$6789']);

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();
        $request = new Request([
            "email" => "uuu@uuu"
        ]);

        $this->userService->user = $user;
        $this->userService->updateEmail($request);

        $request = new Request(['email'=>"uuu@uuu", 'password'=>'test$6789']);
        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();

        $this->assertEquals($user->email_verified_at, false);

    }

    public function create_user()
    {
        $request = new Request([
            'email' => 'test@test',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069',
            'email_verified_at' => Carbon::now(),
        ]);

        $data = $request->validate($this->registeValidation->rules());
        $user = $this->userEntity::store($data);
        $user->email_verified_at = Carbon::now();
        $user->active = 1;
        $user->save();
        $user->assignRole('User');

        return $user;
    }
}
