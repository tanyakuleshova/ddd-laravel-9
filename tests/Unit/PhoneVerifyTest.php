<?php

namespace Tests\Unit;

use App\Domains\Auth\Models\PhoneVerification;
use App\Domains\Auth\Repository\SQLRepository\PhoneVerificationRepository;
use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Auth\Services\Verification\ProcessPhoneVerification;
use App\Domains\SmsProvider\Services\SendSms\SendSMSTwilio;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class PhoneVerifyTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public $phoneVerification;
    public $entity;
    public $validation;
    public $verificationRepository;
    public $smsService;

    public function setUp(): void
    {
        parent::setUp();

        $this->entity = new User();
        $this->validation = new RegisterValidation();
        $this->verificationRepository = new PhoneVerificationRepository();
        $this->userRepository = new UserRepository();
        $this->smsService = new SendSMSTwilio();
        $this->phoneVerification = new ProcessPhoneVerification($this->verificationRepository,  $this->userRepository, $this->smsService);
        Artisan::call('db:seed');
    }

    /** @test */
    public function send_code_successfully()
    {
        $request = new Request([
            'email' => 'test@www',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069'
        ]);

        $data = $request->validate($this->validation->rules());
        $user = $this->entity::store($data);
        $user->assignRole('User');

        $request = new Request(['phone' => $user->phone]);

        $verification = $this->send($request);

        $this->assertEquals($verification->phone, $user->phone);
    }

    /** @test */
    public function send_code_failed()
    {
        $request = new Request(['phone' => '']);

        try {
           $this->send($request);
        }catch(\Exception $exception){

        }
        $this->assertEquals(isset($exception), true);
    }

    /** @test */
    public function verify_wrong_code()
    {
        $request = new Request(['phone' => '0937725069']);

        $this->send($request);

        $request = new Request(['phone' =>'0937725069', 'code'=>'8988']);
        $status =  $this->phoneVerification->process($request);
        $response = json_decode($status->getContent());
        $this->assertEquals($response->error, 'wrong_code');
    }

    /** @test */
    public function verify_expired_code()
    {
        $request = new Request(['phone' => '0937725069']);

        $this->send($request);

        $verification = $this->verificationRepository->firstByParams(['phone' => '0937725069']);
        $verification->created_at = Carbon::now()->subDay();
        $verification->save();

        $request = new Request(['phone' => '0937725069', 'code'=>$verification->code]);
        $status =  $this->phoneVerification->process($request);
        $response = json_decode($status->getContent());

        $this->assertEquals($response->error, 'code_expired');
    }

    public function send($request)
    {
        $request->validate([
            'phone'=> ['required', 'regex:' . User::PHONE_REGEX, 'min:10, max:15'],
        ]);
        $code = $this->phoneVerification->generateCode();
        $this->verificationRepository->deleteByParams(['phone'=>$request->phone]);
        return PhoneVerification::store(['code'=>$code, 'phone'=>$request->phone]);
    }
}
