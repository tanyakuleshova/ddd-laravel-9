<?php

namespace Tests\Unit;


use App\Domains\Auth\Http\Requests\RegisterRequest;
use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Auth\Services\Validation\SocialLoginValidation;
use App\Domains\Auth\Services\Validation\SocialRegisterValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;

use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialAuthTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public $userRepository;
    public $entity;
    public $registerValidation;
    public $loginValidation;
    public $validation;
    public $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
        $this->entity = new User();
        $this->registerValidation = new SocialRegisterValidation();
        $this->loginValidation = new SocialLoginValidation();
        $this->validation = new RegisterValidation();
        $this->service = new CanUserLogin();
        Artisan::call('db:seed');
    }

    /** @test */
    public function register_and_login_successfully()
    {
        $request = new Request([
            'email' => 'test@www',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069',
        ]);

        $data = $request->validate($this->validation->rules());
        $user = $this->entity::store($data);
        $user->assignRole('User');
        $user->email_verified_at = Carbon::now();
        $user->save();
        $user_id = $user->id;

        $this->assertEquals($user->can('user'), true);
        $this->assertEquals($user->email, 'test@www');

        $request = new Request([
            'email' => 'test@www',
            'first_name' => 'TestTest',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069',
            'facebook_token'=>'fhfhgffhfjjgfjgf'
        ]);
        $data = $request->validate($this->registerValidation->rules());
        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        if($user){
            $data['id'] = $user->id;
            $data['active'] = true;
            $user = $this->entity::store($data);
        }else{
            $user = $this->entity::store($data);
            $user->assignRole('User');
        }

        $this->assertEquals($user->id, $user_id);

        $request = new Request([
            'facebook_token' => 'fhfhgffhfjjgfjgf'
        ]);

        $user = $this->userRepository->firstByParams(['facebook_token'=>$request->facebook_token]);
        if(!$user){
            return response()->json(['success'=>false, 'error'=>'user_not_found', 'facebook_token'=>$request->facebook_token]);
        }
        $status = $this->service->check($user);

        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error']]);
        $token = auth('api')->login($user);
        $this->assertEquals(gettype($token), 'string');

    }

    public function login_failed()
    {
        $request = new Request([
            'facebook_token' => 'fhfhgffhfjjgfjgf'
        ]);
        $user = $this->userRepository->firstByParams(['facebook_token'=>$request->facebook_token]);
        $status = '';
        if(!$user){
            $status = response()->json(['success'=>false, 'error'=>'user_not_found', 'facebook_token'=>$request->facebook_token]);
        }
        $this->assertEquals(json_decode($status)->getContent()['success'], false);
    }


}
