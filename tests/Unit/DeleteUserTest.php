<?php

namespace Tests\Unit;

use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Users\Models\Address;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use App\Domains\Users\Services\UserService;
use App\Domains\Users\Services\Validation\UserValidation;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public $userEntity;
    public $addressEntity;
    public $validation;
    public $registeValidation;
    public $userRepository;
    public $userService;


    public function setUp(): void
    {
        parent::setUp();

        $this->userEntity = new User();
        $this->addressEntity = new Address();
        $this->validation = new UserValidation();
        $this->registeValidation = new RegisterValidation();
        $this->userRepository = new UserRepository();
        $this->userService = new UserService($this->addressEntity, $this->userEntity, $this->userRepository);

        Artisan::call('db:seed');
    }

    /** @test */
    public function delete_user_successfully()
    {
        $user = $this->create_user();
        $request = new Request(['email'=>$user->email, 'password'=>'test$6789']);

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $this->userService->user = auth('api')->user();

        $this->userService->deleteUser();
        $user = $this->userRepository->firstByParams(['id'=>$user->id]);
        $this->assertEquals($user->active,false);
    }

    public function create_user()
    {
        $request = new Request([
            'email' => 'test@test',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069',
            'email_verified_at' => Carbon::now(),
        ]);

        $data = $request->validate($this->registeValidation->rules());
        $user = $this->userEntity::store($data);
        $user->email_verified_at = Carbon::now();
        $user->active = 1;
        $user->save();
        $user->assignRole('User');

        return $user;
    }
}
