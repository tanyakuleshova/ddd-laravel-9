<?php

namespace Tests\Unit;

use App\Domains\Auth\Http\Requests\RegisterRequest;
use App\Domains\Auth\Repository\SQLRepository\EmailVerificationRepository;
use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\RecoveryPassword\RecoveryPassword;
use App\Domains\Auth\Services\Validation\PasswordResetValidation;
use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PasswordRecoveryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public $entity;
    public $service;
    public $recoverService;
    public $userRepository;
    public $validation;
    public $registeValidation;

    public function setUp(): void
    {
        parent::setUp();

        $this->entity = new User();
        $this->service = new CanUserLogin();
        $this->validation = new PasswordResetValidation();
        $this->registeValidation = new RegisterValidation();
        $this->userRepository = new UserRepository();
        $this->recoverService = new RecoveryPassword($this->userRepository, $this->entity,  $this->service, $this->validation);

        Artisan::call('db:seed');
    }

    /** @test */
    public function email_link_generated_successfully()
    {
        $user = $this->create_user();

        $request = new Request([
            'email' => $user->email
        ]);

        $request->validate(['email'=>'required']);

        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error']]);

        $passwordReset = $this->recoverService->createPasswordReset($request);

        $this->assertEquals($passwordReset->email, $user->email);
    }

    /** @test */
    public function reset_password_successfully()
    {
        $user = $this->create_user();

        $request = new Request([
            'email' => $user->email
        ]);

        $request->validate(['email'=>'required']);

        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error']]);

        $passwordReset = $this->recoverService->createPasswordReset($request);

        $request = new Request([
            'token' => $passwordReset->token,
            "password" => '212112$eee',
            "password_confirmation" => '212112$eee'
        ]);
        $status = $this->recoverService->process($request);
        $response = json_decode($status->getContent());
        $this->assertEquals($response->success, true);
    }

    /** @test */
    public function reset_password_fail()
    {
        $user = $this->create_user();

        $request = new Request([
            'email' => $user->email
        ]);

        $request->validate(['email'=>'required']);

        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error']]);

        $passwordReset = $this->recoverService->createPasswordReset($request);
        DB::table('password_resets')->where(['email'=>$passwordReset->email])->update(['created_at' => Carbon::now()->subDay()]);

        $request = new Request([
            'token' => $passwordReset->token,
            "password" => '212112$eee',
            "password_confirmation" => '212112$eee'
        ]);
        $status = $this->recoverService->process($request);
        $response = json_decode($status->getContent());
        $this->assertEquals($response->success, false);
    }

    public function create_user()
    {
        $request = new Request([
            'email' => 'test@test',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069',
            'email_verified_at' => Carbon::now(),
        ]);

        $data = $request->validate($this->registeValidation->rules());
        $user = $this->entity::store($data);
        $user->email_verified_at = Carbon::now();
        $user->active = 1;
        $user->save();
        $user->assignRole('User');

        return $user;
    }


}
