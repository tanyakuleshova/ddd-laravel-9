<?php

namespace Tests\Unit;

use App\Domains\Auth\Http\Requests\RegisterRequest;
use App\Domains\Auth\Repository\SQLRepository\EmailVerificationRepository;
use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class EmailVerifyTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public $emailVerification;
    public $entity;
    public $validation;
    public $verificationRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->entity = new User();
        $this->validation = new RegisterValidation();
        $this->verificationRepository = new EmailVerificationRepository();
        $this->userRepository = new UserRepository();
        $this->emailVerification = new ProcessEmailVerification($this->verificationRepository,  $this->userRepository);
        Artisan::call('db:seed');
    }

    /** @test */
    public function send_code_successfully()
    {
        $request = new Request([
            'email' => 'test@www',
            'first_name' => 'Test',
            'last_name' => 'Test',
            'password' => 'test$6789',
            'password_confirmation' => 'test$6789',
            'phone' => '0937725069'
        ]);

        $data = $request->validate($this->validation->rules());
        $user = $this->entity::store($data);
        $user->assignRole('User');

        $request = new Request(['email' => $user->email]);

        $this->emailVerification->send($request);

        $verification = $this->verificationRepository->firstByParams(['email'=>$user->email]);

        $this->assertEquals($verification->email, $user->email);
    }

    /** @test */
    public function send_code_failed()
    {
        $request = new Request(['email' => '']);

        try {
        $this->emailVerification->send($request);
        }catch(\Exception $exception){

        }
        $this->assertEquals(isset($exception), true);
    }

    /** @test */
    public function verify_wrong_code()
    {
        $request = new Request(['email' => 'test@test']);

        $this->emailVerification->send($request);

        $request = new Request(['email' =>'test@test', 'code'=>'8988']);
        $status =  $this->emailVerification->process($request);
        $response = json_decode($status->getContent());
        $this->assertEquals($response->error, 'wrong_code');
    }

    /** @test */
    public function verify_expired_code()
    {
        $request = new Request(['email' => 'test@test']);

        $this->emailVerification->send($request);

        $verification = $this->verificationRepository->firstByParams(['email'=>'test@test']);
        $verification->created_at = Carbon::now()->subDay();
        $verification->save();

        $request = new Request(['email' =>'test@test', 'code'=>$verification->code]);
        $status =  $this->emailVerification->process($request);
        $response = json_decode($status->getContent());

        $this->assertEquals($response->error, 'code_expired');
    }
}
