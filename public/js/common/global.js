let lib = {
    borderColor: '#ced4da',
    highlightClass: 'bg-gray-200'
}

window.onload = function () {
    defineActiveLink();
    escapeHandler();
}

function openModal(classname, modal_name, data=''){
    let modal = document.querySelector('.' + classname);
    if(modal){
        clearModal(modal);
        modal.classList.remove('hidden');
        if(data){
            fill_modal(modal, modal_name, data);
        }else{
            setModalName(modal, modal_name)
        }
    }
}

function closeModal(classname){
    let modal = document.querySelector('.' + classname);
    if(modal){
        modal.classList.add('hidden');
    }
}
function fill_modal(modal, name, obj){
console.log(obj);
    for (let key in obj) {
        let field = modal.querySelector('input[name="'+key+'"]');
        if(field){
            field.value = obj[key];
        }else{
            let field = modal.querySelector('textarea[name="'+key+'"]');
            if(field) {
                field.innerText = obj[key];
            }else{
                let select = modal.querySelector('select[name="'+key+'"]');
                if(select){
                    select.value = obj[key];
                }
            }
        }
    }
    let title = modal.querySelector('.modal_name');
    if(title && obj.id){
        title.innerText =  name + ' id ' + obj.id;
    }
}

function setModalName(modal, name)
{
    let title = modal.querySelector('.modal_name');
    if(title){
        title.innerText =  name;
    }
}

function clearModal(modal){
    let errors = modal.querySelectorAll('.error');
    let fields = modal.querySelectorAll('input');
    let text = modal.querySelectorAll('textarea');
    let field = document.querySelector('.error_message');

    if(fields){
        fields.forEach(function (item){
            item.value = '';
            item.style.borderColor = lib.borderColor;
        })
    }

    if(text){
        text.forEach(function (item){
            item.innerText = '';
            item.style.borderColor = lib.borderColor;
        })
    }

    if(errors){
        errors.forEach(function (item){
            item.innerHTML = '';
        })
    }

    if(field){
        field.innerText = '';
    }
}

function closeAllModal() {
    let modal = document.querySelectorAll('.modal_window');
    if(modal){
        modal.forEach(function (item){
            item.classList.add('hidden');
        })

    }
}

function serializeForm (form){
    let obj = {};
    const formData = new FormData(form);
    for (let key of formData.keys()) {
        obj[key] = formData.get(key);
    }
    return obj;
}

function handleModalData(classname, url, method)
{
    let csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    let modal = document.querySelector('.'+classname);
    let form = document.querySelector('.'+classname + ' form');
    if(form){
        let data = serializeForm(form);

        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRF-TOKEN': csrf
            },
            body: JSON.stringify(data)
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                if (data.error) {
                    if(data.error === 'validation_error'){
                        handleValidationErrors(modal, data);
                    }else{
                        let field = document.querySelector('.error_message');
                        if(field && data.message){
                            field.innerText = data.message;
                        }
                    }
                }else if(data.success){
                    if(data.redirect){
                        location.href = data.redirect;
                    }
                    updateGrid(data.view);
                    closeAllModal();
                    highlightRow(data.instance.id);
                } else {
                    console.log('Something went wrong. Please try again')
                }
            })
    }
}

function handleValidationErrors(modal, data)
{
    for (let key in data.errors) {
        let field = modal.querySelector('[name="'+key+'"]');
        if(field){
            field.style.borderColor = "red";
            let error_field = field.parentElement.querySelector('.error');
            if(error_field){
                let html = '';
                data.errors[key].forEach(function (item){
                    let li = '<li>' + item +'</li>';
                    html += li;
                });
                error_field.innerHTML = html;
            }
        }
    }
}

function updateGrid(view)
{
    let grid = document.querySelector('.grid_container');
    if(grid){
        grid.innerHTML = view;
    }
}
function updatePage(view)
{
    let grid = document.querySelector('.grid_page');
    if(grid){
        grid.innerHTML = view;
    }
}

function highlightRow(id)
{
    let row = document.querySelector('[data-row="'+id+'"]');
    if(row){
        row.classList.add(lib.highlightClass);
        setTimeout(function (){
            row.classList.remove(lib.highlightClass);
        }, 1000);
    }
}

function defineActiveLink(){
    let name = location.pathname.split('/');
    let link = document.querySelector('[data-name="'+name[1]+'"]');
    if(link){
        link.classList.add('text-blue');
    }
}
function escapeHandler()
{
    document.addEventListener('keyup', function(e) {
        if (e.key === "Escape") {
            closeAllModal();
        }
    });
}

