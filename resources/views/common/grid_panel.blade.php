<div class="p-4">
    <form class="flex items-end search_form form_form">
        <div class="mr-2">
            <label>Field</label>
            <select name="field" id="" class="form-control" style="min-width: 200px">
                @foreach($fields as $field)
                    <option value="{{$field}}">{{$field}}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label>Value</label>
            <input name="value" type="text" placeholder="value" class="form-control rounded-md">
        </div>
        <div class="btn bg-blue-400 hover:bg-blue-500 mx-2" onclick="searchRequest('search_form')">Search</div>
        <div class="btn bg-gray-400 hover:bg-gray-500" onclick="clearSearchRequest()">Clear</div>
    </form>
</div>
